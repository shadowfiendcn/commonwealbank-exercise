##CBAExercise Project 

###Environment
Xcode 7.2 with Swift 2

###Notes
1. Use Cocoapods to install all the dependences
2. The app can either pull data from the local json file or remote file, just change the following line in AccountService.swift 
 
	```
	var source = AccountSerivceSource.RemoteService;
	```
3. The dynamic height of the transaction cell is not implemented

###Basic Features
1. Show a list of transactions corresponding to an account (iOS.Transactions.png)
2. Example data has been included (exercise.json)
3. On this screen, the account information on top scrolls off the screen (iOS.Transactions.scrolling.png)
4. Note that the transactions (both cleared and pending) should be shown together in descending date order

###Bonus Marks
1. Group transactions by date
2. Some transactions will be ATM withdrawals. These rows should be indicated by the location icon
3. Tapping on an ATM withdrawal row will show the location of the ATM on a map (iOS.Map.png)
4. a universal app supporting both orientations
5. Dynamically pull down data from https://www.dropbox.com/s/tewg9b71x0wrou9/data.json?dl=1

###Extra Features
1. Pull to refresh