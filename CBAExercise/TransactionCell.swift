//
//  TransactionCell.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {

    @IBOutlet private var descriptionLabel:UILabel?;
    @IBOutlet private var amountLabel:UILabel?;
    @IBOutlet private var findUsImageView:UIImageView?;
    @IBOutlet private var showFindUsImageViewConstraint:NSLayoutConstraint?;
    @IBOutlet private var hideFindUsImageViewConstraint:NSLayoutConstraint?;
    
    private var numberFormatter = NSNumberFormatter();
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.numberFormatter.numberStyle = .CurrencyStyle;
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(transaction transaction:Transaction, isPending isPendingTransaction:Bool){
        
        if (isPendingTransaction){
            let prefix = NSLocalizedString("PENDING: ", comment: "");
            let attributedDescription = NSMutableAttributedString();
            
            attributedDescription.appendAttributedString(NSAttributedString(string: prefix, attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(12)]));
            
            if let filteredDescription = self.filteredTransactionDescription(transaction.transactionDescription) {
                attributedDescription.appendAttributedString(NSAttributedString(string:filteredDescription, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(12)]));
            }
            self.descriptionLabel?.attributedText = attributedDescription;
            
        }
        else {
            self.descriptionLabel?.text = self.filteredTransactionDescription(transaction.transactionDescription);
            
        }
        
        self.amountLabel?.text = self.numberFormatter.stringFromNumber(NSNumber(double:transaction.amount));
        
        if (transaction.atmId != nil){
            self.findUsImageView?.hidden = false;
            self.showFindUsImageViewConstraint?.priority = UILayoutPriorityDefaultHigh;
            self.hideFindUsImageViewConstraint?.priority = UILayoutPriorityDefaultLow;
        }
        else {
            self.findUsImageView?.hidden = true;
            self.showFindUsImageViewConstraint?.priority = UILayoutPriorityDefaultLow;
            self.hideFindUsImageViewConstraint?.priority = UILayoutPriorityDefaultHigh;
        }
    }
    
    //MARK: Helper
    func filteredTransactionDescription(transactionDescription:String?)->String?{
        // is this transaction description html? another other special characters?
        // at the moment we only replace the <br/> character
        return transactionDescription?.stringByReplacingOccurrencesOfString("<br/>", withString: "\n");
    }
}
