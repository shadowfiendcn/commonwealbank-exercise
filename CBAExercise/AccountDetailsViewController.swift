//
//  ViewController.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import UIKit

class AccountDetailsViewController: UITableViewController {

    var headerView = AccountDetailsHeaderView.viewFromXib();
    
    var accountService = AccountService();
    
    var exerciseData:ExerciseData?;
    var transactionDateList:[NSDate]?;
    var groupedTransactionDict:[NSDate:[Transaction]]?;
    
    var currentDate = NSDate();
    
    private var numberFormatter = NSNumberFormatter();
    
    let transactionSectionHeaderViewIdentifier = "TransactionSectionHeaderView";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.headerView.autoresizingMask = [.FlexibleWidth];
        
        self.refreshControl = UIRefreshControl();
        self.refreshControl!.addTarget(self, action: "loadRemoteData", forControlEvents: .ValueChanged);
        
        self.headerView.hidden = true;
        self.tableView.tableHeaderView = self.headerView;
        self.tableView.tableFooterView = UIView(frame: CGRectZero);
        self.tableView.registerNib(UINib.init(nibName: transactionSectionHeaderViewIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: transactionSectionHeaderViewIdentifier);
        
        self.numberFormatter.numberStyle = .CurrencyStyle;
        
        self.loadRemoteData();
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table View
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let transactionList = self.transactionListInSection(section) {
            return transactionList.count;
        }
        else {
            return 0;
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let transactionDateList = self.transactionDateList {
            return transactionDateList.count;
        }
        else {
            return 0;
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TransactionCell", forIndexPath: indexPath) as! TransactionCell;
        
        if let transactionList = self.transactionListInSection(indexPath.section) {
            
            let transaction = transactionList[indexPath.row];
            
            if let isPendingTransaction = self.exerciseData?.pending.contains({$0 === transaction}) {
                cell.config(transaction: transaction, isPending: isPendingTransaction);
            }
        }
        
        return cell;
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20;
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //  dynamic table view height can be implemetned using a prototype cell
        //  and call systemLayoutSizeFittingSize(UILayoutFittingCompressedSize);
        return 44;
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView = tableView.dequeueReusableHeaderFooterViewWithIdentifier(transactionSectionHeaderViewIdentifier);
        return sectionHeaderView;
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let sectionHeaderView = view as! TransactionSectionHeaderView;
        
        if let effectiveDate = self.transactionDateList?[section] {
            sectionHeaderView.config(effectiveDate, current: self.currentDate);
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let transactionList = self.transactionListInSection(indexPath.section) {
            
            let transaction = transactionList[indexPath.row];
            
            if (transaction.atmId != nil){
                if let atm = self.exerciseData?.atms.filter({$0.id == transaction.atmId}).first{
                    self.performSegueWithIdentifier("showLocation", sender: atm);
                    return;
                }
            }
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true);
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let atmLocationViewController = segue.destinationViewController as? ATMLocationViewController {
            atmLocationViewController.atm = sender as? ATM;
        }
    }
    
    //MARK: Data
    func loadRemoteData(){
        
        // show loading
        if(!self.refreshControl!.refreshing){
            self.refreshControl!.beginRefreshing();
            self.tableView.contentOffset = CGPointMake(0,-44);
        }
        
        self.accountService .getAccountDetails {[weak self] (exerciseData, error) -> Void in
            if (error != nil){
                // present error
                let alertController = UIAlertController(title: NSLocalizedString("Failed to load the account details", comment: ""), message: error?.localizedDescription, preferredStyle: .Alert);
                alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .Default, handler: nil));
                self?.presentViewController(alertController, animated: true, completion: { () -> Void in
                    self?.refreshControl?.endRefreshing();
                });
            }
            else {
                
                self?.populateExerciseData(exerciseData!, complete: { () -> Void in
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), { () -> Void in
                        self?.refreshControl?.endRefreshing();
                    });
                });
            }
        }
    }
    
    func transactionListInSection(section:Int)->[Transaction]?{
        if let effectiveDate = self.transactionDateList?[section] {
            let transactionList = self.groupedTransactionDict?[effectiveDate];
            return transactionList;
        }
        else {
            return nil;
        }
    }
    
    func populateExerciseData(exerciseData:ExerciseData, complete:()->Void){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {[weak self] () -> Void in
            
            self?.exerciseData = exerciseData;
            
            // data updates
            var transactionList:[Transaction] = [];
            transactionList.appendContentsOf(exerciseData.transactions);
            transactionList.appendContentsOf(exerciseData.pending);
            
            if let (transactionDateList, groupedTransactionDict) = self?.groupedTransactionListFromList(transactionList){
                
                self?.transactionDateList = transactionDateList;
                self?.groupedTransactionDict = groupedTransactionDict;
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    // UI updates
                    self?.headerView.hidden = false;
                    self?.headerView.accountNameLabel?.text = exerciseData.account?.accountName;
                    self?.headerView.accountNumberLabel?.text = exerciseData.account?.accountNumber;
                    if let account = exerciseData.account {
                        self?.headerView.accountBalanceLabel?.text = self?.numberFormatter.stringFromNumber(NSNumber(double: account.balance));
                        self?.headerView.availableFundsLabel?.text = self?.numberFormatter.stringFromNumber(NSNumber(double: account.available));
                    }
                    else {
                        self?.headerView.accountBalanceLabel?.text = "";
                        self?.headerView.availableFundsLabel?.text = "";
                    }
                    
                    self?.tableView.reloadData();
                    complete();
                });
            }
        };
    }
    
    func groupedTransactionListFromList(transactionList:[Transaction])->(transactionDateList:[NSDate], groupedTranscationDict:[NSDate:[Transaction]]){
        var transactionDateList:[NSDate] = [];
        var groupedTransactionDict = [NSDate:[Transaction]]();
        for transaction in transactionList{
            if let effectiveDate = transaction.effectiveDate{
                if (groupedTransactionDict[effectiveDate] == nil){
                    groupedTransactionDict[effectiveDate] = [];
                    transactionDateList.append(effectiveDate);
                }
                groupedTransactionDict[effectiveDate]!.append(transaction);
            }
        }
        
        transactionDateList.sortInPlace({ $1.compare($0) == NSComparisonResult.OrderedAscending })
        
        return (transactionDateList, groupedTransactionDict);
    }
}

