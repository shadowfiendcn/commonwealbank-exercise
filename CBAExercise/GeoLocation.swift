//
//  Location.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import ObjectMapper


class GeoLocation: Mappable {
    var lat:Double = 0;
    var lng:Double = 0;

    required init?(_ map: Map) {

    }

    // Mappable
    func mapping(map: Map) {
        lat    <- map["lat"]
        lng    <- map["lng"]
    }
}