//
//  AccountService.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire


class AccountService {
    
    
    enum AccountServiceError: Int {
        case FileNotFound
        case InvalidJSONFile
        case NetworkFailure
        case InvalidSource
    }
    
    enum AccountSerivceSource: Int {
        case LocalFile
        case RemoteService
    }
    
    // switch the source for the inital load here
    var source = AccountSerivceSource.LocalFile;
    
    static var AccountServiceErrorDomain = "AccountServiceErrorDomain";
    
    func getAccountDetails(callback:(ExerciseData?, NSError?)->Void){
        if (self.source == AccountSerivceSource.LocalFile){
            self.getAccountDetailsFromLocalFile(callback);
        }
        else if (self.source == AccountSerivceSource.RemoteService){
            self.getAccountDetailsFromRemoteService(callback);
        }
        else {
            callback(nil, NSError(domain: AccountService.AccountServiceErrorDomain, code: AccountServiceError.InvalidSource.rawValue, userInfo: [NSLocalizedDescriptionKey:NSLocalizedString("Invalid source", comment: "")]));
        }
    }
    
    func parseJSONString(jsonString:String, callback:(ExerciseData?, NSError?)->Void){
        if let exerciseData = Mapper<ExerciseData>().map(jsonString){
            callback(exerciseData, nil);
        }
        else {
            callback(nil, NSError(domain: AccountService.AccountServiceErrorDomain, code: AccountServiceError.InvalidJSONFile.rawValue, userInfo: [NSLocalizedDescriptionKey:NSLocalizedString("Invalid json file", comment: "")]));
        }
    }
    
    func getAccountDetailsFromRemoteService(callback:(ExerciseData?, NSError?)->Void){
        let serviceUrl = "https://www.dropbox.com/s/tewg9b71x0wrou9/data.json?dl=1";
        Alamofire.request(.GET, serviceUrl).responseString { response in
            if let error = response.result.error{
                callback(nil, NSError(domain: AccountService.AccountServiceErrorDomain, code: AccountServiceError.NetworkFailure.rawValue, userInfo: [NSLocalizedDescriptionKey:error.localizedDescription]));
            }
            else if let jsonString = response.result.value{
                self.parseJSONString(jsonString, callback: callback);
            }
            else {
                callback(nil, NSError(domain: AccountService.AccountServiceErrorDomain, code: AccountServiceError.NetworkFailure.rawValue, userInfo: [NSLocalizedDescriptionKey:NSLocalizedString("No string returned from remote server", comment: "")]));
            }
        };
    }
    
    func getAccountDetailsFromLocalFile(callback:(ExerciseData?, NSError?)->Void){
        
        if let path = NSBundle.mainBundle().pathForResource("exercise", ofType: "json"){
            
            do {
                let jsonString = try String(contentsOfFile: path, encoding: NSUTF8StringEncoding);
                self.parseJSONString(jsonString, callback: callback);
            }
            catch let error as NSError {
                callback(nil, error);
            }
        }
        else {
            callback(nil, NSError(domain: AccountService.AccountServiceErrorDomain, code: AccountServiceError.FileNotFound.rawValue, userInfo: [NSLocalizedDescriptionKey:NSLocalizedString("The json file cannot be found", comment: "")]));
        }
    }
}