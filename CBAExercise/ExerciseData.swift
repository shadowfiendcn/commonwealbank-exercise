//
//  ExerciseData.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import ObjectMapper

class ExerciseData:Mappable{
    
    var account:Account?;
    var transactions:[Transaction] = [];
    var pending:[Transaction] = [];
    var atms:[ATM] = [];

    required init?(_ map: Map) {

    }
    
    func mapping(map: Map) {
        account         <- map["account"]
        transactions    <- map["transactions"]
        pending         <- map["pending"]
        atms            <- map["atms"]
    }
    
    
}