//
//  ATMLocationViewController.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import UIKit
import MapKit

class ATMAnnotation:NSObject, MKAnnotation {
    let atm:ATM;
    let coordinate: CLLocationCoordinate2D
    
    init(atm:ATM){
        self.atm = atm;
        if let location = atm.location{
            self.coordinate = CLLocationCoordinate2DMake(location.lat, location.lng);
        }
        else {
            self.coordinate = CLLocationCoordinate2DMake(0, 0);
        }
    }
}

class ATMLocationViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet private var mapView:MKMapView?;
    
    var atm:ATM?;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let location = self.atm?.location {
            self.centerMapOnLocation(CLLocation(latitude: location.lat, longitude: location.lng))
        }
        
        if let atm = self.atm {
            let annotation = ATMAnnotation(atm:atm);
            self.mapView?.addAnnotation(annotation);
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: MKMapViewDelegate
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? ATMAnnotation{
            let identifier = "atm";
            if let annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier){
                annotationView.annotation = annotation;
                return annotationView;
            }
            else {
                let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier);
                annotationView.image = UIImage(named: "CBAFindUsAnnotationIconATM");
                return annotationView;
            }
        }
        return nil;
    }

    //MARK: Helper
    func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 300;
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        self.mapView?.setRegion(coordinateRegion, animated: true)
    }

}
