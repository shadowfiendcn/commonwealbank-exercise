//
//  Transaction.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import ObjectMapper;

class Transaction:Mappable{
    
    var id:String?;
    var effectiveDate:NSDate?;
    var transactionDescription:String?;
    var atmId:String?;
    var amount:Double = 0;
    var dateFormatter = NSDateFormatter();
    
    required init?(_ map: Map) {
        dateFormatter.dateFormat = "dd/mm/yyyy";
    }
    
    // Mappable
    func mapping(map: Map) {
        id                  <- map["id"]
        effectiveDate       <- (map["effectiveDate"], DateFormatterTransform(dateFormatter: self.dateFormatter))
        transactionDescription  <- map["description"]
        atmId               <- map["atmId"]
        amount              <- map["amount"]
    }
}