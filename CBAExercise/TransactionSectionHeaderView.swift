//
//  TransactionSectionHeaderView.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import UIKit

class TransactionSectionHeaderView: UITableViewHeaderFooterView {

    @IBOutlet private var leftTitleLabel:UILabel?;
    @IBOutlet private var rightTitleLabel:UILabel?;
    
    private var dateFormatter = NSDateFormatter();

    override func awakeFromNib() {
        self.dateFormatter.dateStyle = .MediumStyle;
    }
    
    func config(effectiveDate:NSDate, current currentDate:NSDate){
        
        self.leftTitleLabel?.text = self.dateFormatter.stringFromDate(effectiveDate);
        
        let numberOfDaysPassed = self.numberOfDaysBetween(effectiveDate, and: currentDate);
        self.rightTitleLabel?.text = self.stringFromNumberOfDaysPassed(numberOfDaysPassed);
    }
    
    func stringFromNumberOfDaysPassed(numberOfDaysPassed:Int)->String{
        let string:String;
        if (numberOfDaysPassed == 0){
            string = NSLocalizedString("Today", comment: "");
        }
        else if (numberOfDaysPassed == 1){
            string = NSLocalizedString("1 Day Ago", comment: "");
        }
        else if (numberOfDaysPassed > 1){
            string = String(format: "%d Days Ago", numberOfDaysPassed);
        }
        else {
            // transaction in the future, no display logic yet
            string = "";
        }
        return string;
    }
    
    func numberOfDaysBetween(fromDate:NSDate, and toDate:NSDate)->Int{
        let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian);
        let components = calendar?.components(.Day, fromDate: fromDate, toDate:toDate, options: .WrapComponents);
        return components!.day;
        
    }
}
