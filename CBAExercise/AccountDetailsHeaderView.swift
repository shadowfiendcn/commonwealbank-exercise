//
//  AccountDetailsHeaderView.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import UIKit

class AccountDetailsHeaderView: UIView {

    @IBOutlet var accountNameLabel:UILabel?;
    @IBOutlet var accountNumberLabel:UILabel?;
    @IBOutlet var availableFundsLabel:UILabel?;
    @IBOutlet var accountBalanceLabel:UILabel?;
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    static func viewFromXib()->AccountDetailsHeaderView{
        return NSBundle.mainBundle().loadNibNamed("AccountDetailsHeaderView", owner: nil, options: nil).first as! AccountDetailsHeaderView;
    }

}
