//
//  ATM.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import ObjectMapper

class ATM:Mappable{
    var id:String?;
    var name:String?;
    var address:String?;
    var location:GeoLocation?;
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id      <- map["id"]
        name    <- map["name"]
        address <- map["address"]
        location <- map["location"]
    }
    
}