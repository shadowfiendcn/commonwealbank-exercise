//
//  Account.swift
//  CBAExercise
//
//  Created by Mei Ma on 7/01/2016.
//  Copyright © 2016 Mei Ma. All rights reserved.
//

import Foundation
import ObjectMapper

class Account: Mappable{
    
    var accountName:String?;
    var accountNumber:String?;
    var available:Double = 0;
    var balance:Double = 0;
    
    required init?(_ map: Map) {

    }
    
    func mapping(map: Map) {
        accountName     <- map["accountName"]
        accountNumber   <- map["accountNumber"]
        available       <- map["available"]
        balance         <- map["balance"]
    }
}